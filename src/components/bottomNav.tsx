import * as React from 'react';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';

import AddchartOutlinedIcon from '@mui/icons-material/AddchartOutlined';
import AssessmentOutlinedIcon from '@mui/icons-material/AssessmentOutlined';
import SettingsIcon from '@mui/icons-material/Settings';
import {Paper} from "@mui/material";
//import {useRouter} from "next/router";

export default function MenuBottomNavigation({page = "" , changeRoute}  : {page:any, changeRoute:any}) {
    //const [value, setValue] = React.useState(page || 'dashboard');

    //console.log(page);

    //const router = useRouter();
    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
        //setValue(newValue);
        changeRoute(newValue);
    };

    return (
        <Paper sx={{ position: 'fixed', bottom: 0, left: 0, right: 0 }} elevation={3}>
            <BottomNavigation sx={{ width: "100%" }} value={page || "dashboard"} onChange={handleChange}>
                <BottomNavigationAction
                    label="Dashboard"
                    value="dashboard"
                    icon={<AssessmentOutlinedIcon />}
                />
                <BottomNavigationAction
                    label="About us"
                    value="about"
                    icon={<AddchartOutlinedIcon />}
                />
                <BottomNavigationAction
                    label="Settings"
                    value="settings"
                    icon={<SettingsIcon />}
                />

            </BottomNavigation>
        </Paper>
    );
}