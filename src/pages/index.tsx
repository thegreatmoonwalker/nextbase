import type {NextPage} from 'next';
import Head from 'next/head';
import styles from '../../styles/Home.module.css';
import Box from "@mui/material/Box";
import  {useEffect} from "react";

import Grid from "@mui/material/Grid";
import Layout from "../components/layout";
import Link from "next/link";
import {Button} from "@mui/material";


const Home: NextPage = () => {

    useEffect(() => {
        // Remove the server-side injected CSS.
        const jssStyles: any = document.querySelector('#jss-server-side');
        if (jssStyles) {
            jssStyles.parentElement.removeChild(jssStyles);
        }
    }, []);

    return (
        <Layout>


            <div className={styles.container}>
                <Head>
                    <title>Moon Bot2</title>
                    <meta name="description" content="Dev : @thegreatmoonwalker"/>
                    <link rel="icon" href="/favicon.ico"/>
                </Head>

                <Box className={styles.main}>

                    <Grid container >
                        <Grid item xs={12}>
                            <Link href={"/other-page"} ><Button>Go to other page</Button></Link>
                        </Grid>

                    </Grid>
                </Box>

            </div>
        </Layout>

    );
};

export default Home;
